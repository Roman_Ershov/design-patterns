package factories;

import buttons.WindowsButton;
import checkboxes.WindowsCheckBox;

public class WindowsFactory implements GUIFactory {

	@Override
	public WindowsButton createButton() {
		return new WindowsButton();
	}

	@Override
	public WindowsCheckBox createCheckBox() {
		return new WindowsCheckBox();
	}
	
}
