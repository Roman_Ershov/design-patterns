package factories;

import buttons.Button;
import checkboxes.CheckBox;

public interface GUIFactory {
	public Button createButton();
	public CheckBox createCheckBox();
}
