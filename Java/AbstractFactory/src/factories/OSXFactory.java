package factories;

import buttons.OSXButton;
import checkboxes.OSXCheckBox;

public class OSXFactory implements GUIFactory {

	@Override
	public OSXButton createButton() {
		return new OSXButton();
	}

	@Override
	public OSXCheckBox createCheckBox() {
		return new OSXCheckBox();
	}
	
}
