package demo;
import factories.*;

public class Demo {
	public static Application configureApp(){
		Application app;
		GUIFactory factory;
		String os = System.getProperty("os.name");
		
		if (os.equals("Mac OS X")){
			factory = new OSXFactory();
		}
		else{
			factory = new WindowsFactory();
		}
		
		app = new Application(factory);
		return app;
	}
	
	public static void main(String[] args){
		Application app = configureApp();
		app.paint();
	}
}
