package buttons;

public interface Button {
	public void render();
	public void onClick();
}
