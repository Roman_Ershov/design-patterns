package demo;
import factory.*;

public class Demo {

	private static Dialog dialog;
	
	static void configure(){
		String sys = System.getProperty("os.name");
		if (sys.equals("Windows 8.1")){
			dialog = new WindowsDialog();
		} else {
			dialog = new HtmlDialog();
		}
	}
	
	static void runBusinessLogic(){
		dialog.renderWindow();
	}
	
	public static void main(String[] args) {
		configure();
		runBusinessLogic();
	}

}
