package demo;
import numbers.Number;

public class Demo {

	public static void main(String[] args) {
		Number.getInstance().setValue(42);
		System.out.println("value is " + 
				Integer.toString(Number.getInstance().getValue()));
		
		Number.setType("octal");
		Number.getInstance().setValue(64);
		System.out.println("value is " + 
				Integer.toString(Number.getInstance().getValue()));
	}

}
