package numbers;

public class Octal extends Number{
	protected Octal(){}
	
	@Override
	public void setValue(int value){
		String oct_str = Integer.toOctalString(value);
		this.value = Integer.parseInt(oct_str);
	}
}
