package numbers;

public class Number {

	protected int value;
	
	private static String type = "decimal";
	
	private static Number instance = null;
	
	protected Number(){
		System.out.println("Number constructor");
	}
	
	public static Number getInstance(){
		if (instance == null){
			if (type == "octal"){
				instance = new Octal();
			}
			else{
				instance = new Number();
			}
		}
		
		return instance;
	}
	
	public static void setType(String t){
		type = t;
		instance = null;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	
}
