package stack;
import stack_impl.*;

public class StackFIFO extends Stack {
	private StackImpl temp = new StackList();
	
	public StackFIFO(){
		super("array");
	}
	
	public int pop(){
		while(!isEmpty()){
			temp.push(super.pop());
		}
		
		int ret = temp.pop();
		while(!temp.isEmpty()){
			push(temp.pop());
		}
		
		return ret;
	}
}
