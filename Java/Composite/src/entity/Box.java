package entity;

import java.util.ArrayList;
import java.util.List;

public class Box extends Entity {

	private List<Entity> children = new ArrayList<>();
	private int value;
	
	public Box(int value) {
		super();
		this.value = value;
	}

	public void Add(Entity c){
		children.add(c);
	}

	@Override
	public void traverse() {
		System.out.println(indent.toString() + value);
		indent.append(" ");
		for (int i = 0; i < children.size(); i++){
			children.get(i).traverse();
		}
		//indent.setLength(indent.length() - 3);		
	}
	
}
