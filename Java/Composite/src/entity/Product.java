package entity;

public class Product extends Entity {

	private int value;
	
	public Product(int value) {
		super();
		this.value = value;
	}

	@Override
	public void traverse() {
		System.out.println(indent.toString() + value);
	}

}
