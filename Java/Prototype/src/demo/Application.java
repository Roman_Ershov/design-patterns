package demo;
import java.util.ArrayList;

import shapes.*;


public class Application {
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	
	public Application(){
		Circle circle = new Circle(10, 20, 15, "black");
		shapes.add(circle);
		Circle anotherCircle = (Circle)circle.Clone();
		shapes.add(anotherCircle);
		
		Rectangle rectangle = new Rectangle(5, 10, 10, 20, "red");
		shapes.add(rectangle);
	}
	
	public ArrayList<Shape> GetShapes(){
		return shapes;
	}
}
