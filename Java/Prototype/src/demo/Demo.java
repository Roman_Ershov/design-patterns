package demo;
import shapes.Shape;
import java.util.ArrayList;

public class Demo {

	public static void main(String[] args) {
		Application app = new Application();
		ArrayList<Shape> shapes = app.GetShapes();
		
		ArrayList<Shape> shapesCopy = new ArrayList<Shape>();
		for (Shape shape : shapes){
			shapesCopy.add(shape.Clone());
		}
	}

}
