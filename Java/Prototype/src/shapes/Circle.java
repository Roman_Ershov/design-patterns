package shapes;

public class Circle extends Shape {

	private int radius;
	
	public Circle(int rad, int x, int y, String col){
		super(x, y, col);
		this.radius = rad;
	}
	
	public Circle(Circle target){
		super(target);
		if (target != null){
			this.radius = target.radius;
		}
		
		System.out.println("Copy Circle: " + target.X + " " + target.Y + " " + 
		target.color + " " + target.radius);
	}
	
	@Override
	public Shape Clone() {
		return new Circle(this);
	}
	
}
