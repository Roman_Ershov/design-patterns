package shapes;

public abstract class Shape {
	protected int X;
	protected int Y;
	protected String color;
	
	public Shape(int x, int y, String col){
		this.X = x;
		this.Y = y;
		this.color = col;
	}
	
	public Shape(Shape target){
		if (target != null){
			this.X = target.X;
			this.Y = target.Y;
			this.color = target.color;
		}
	}
	
	public abstract Shape Clone();
}
