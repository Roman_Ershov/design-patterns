package shapes;

public class Rectangle extends Shape {

	private int width;
	private int height;
	
	public Rectangle(int w, int h, int x, int y, String col){
		super(x, y, col);
		this.width = w;
		this.height = h;
	}
	
	public Rectangle(Rectangle target){
		super(target);
		if (target != null){
			this.width = target.width;
			this.height = target.height;
		}
		
		System.out.println("Copy Rectangle: " + target.X + " " + target.Y + " " + 
		target.color + " " + target.width + target.height);
	}
	
	@Override
	public Shape Clone() {
		return new Rectangle(this);
	}
	
}
