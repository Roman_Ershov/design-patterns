package demo;
import director.Director;
import builders.CarBuilder;
import builders.CarManualBuilder;
import cars.*;

public class Demo {

	public static void main(String[] args) {
		Director director = new Director();
		
		CarBuilder builder = new CarBuilder();
		director.constructSportsCar(builder);
		
		Car car = builder.getResult();
		System.out.println("Car built:\n" + car.getType());
		
		CarManualBuilder manualBuilder = new CarManualBuilder();
		director.constructCityCar(manualBuilder);
		Manual carManual = manualBuilder.getResult();
		System.out.println("\nCar manual built:\n" + carManual.print());
	}

}
