#include "WordApplication.h"
#include "DocDocument.h"

WordApplication::WordApplication()
{
	cout << "WordApplication: Constructor" << endl;
}

WordApplication::~WordApplication()
{
}

Document* WordApplication::CreateDocument(const string& name)
{
	cout << "WordApplication: CreateDocument()" << endl;
	return new DocDocument(name);
}