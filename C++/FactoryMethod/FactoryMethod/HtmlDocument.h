#pragma once
#include "Document.h"
class HtmlDocument :
	public Document
{
public:
	HtmlDocument(const string &name) : Document(name){}

	~HtmlDocument(){}

	void Open();

	void Close();
};

