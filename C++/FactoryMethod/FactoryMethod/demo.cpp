#include "WordApplication.h"
#include "ChromeApplication.h"

int main(int argc, char* argv[])
{
	WordApplication app;

	string name;

	for (int i = 0; i < 5; i++)
	{
		name.clear();
		name += "Document";
		name += std::to_string(i + 1);
		name += ".doc";

		app.NewDocument(name);
	}

	app.ReportDocs();
}