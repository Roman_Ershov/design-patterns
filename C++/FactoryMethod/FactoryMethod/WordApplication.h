#pragma once
#include "Application.h"
class WordApplication :
	public Application
{
	Document* CreateDocument(const string& name);

public:
	WordApplication();
	~WordApplication();
};

