#pragma once
#include "Application.h"
class ChromeApplication :
	public Application
{

	Document* CreateDocument(const string& name);

public:
	ChromeApplication();
	~ChromeApplication();
};

