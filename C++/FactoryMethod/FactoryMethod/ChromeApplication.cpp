#include "ChromeApplication.h"
#include "HtmlDocument.h"

ChromeApplication::ChromeApplication()
{
	cout << "ChromeApplication: Constructor" << endl;
}


ChromeApplication::~ChromeApplication()
{
}

Document* ChromeApplication::CreateDocument(const string& name)
{
	cout << "ChromeApplication: CreateDocument" << endl;
	return new HtmlDocument(name);
}