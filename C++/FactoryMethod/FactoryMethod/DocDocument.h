#pragma once
#include "Document.h"
class DocDocument :
	public Document
{
public:
	DocDocument(const string& name) : Document(name) {}
	~DocDocument();

	void Open();
	void Close();
};

