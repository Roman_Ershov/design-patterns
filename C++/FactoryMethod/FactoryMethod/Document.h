#pragma once
#include <iostream>
#include <string>
using namespace std;

class Document
{
	string name;

public:
	Document(const string& name)
	{
		this->name = name;
	}

	virtual void Open() = 0;
	virtual void Close() = 0;

	string GetName()
	{
		return name;
	}

	virtual ~Document()
	{
		name.clear();
		name.shrink_to_fit();
	}
};