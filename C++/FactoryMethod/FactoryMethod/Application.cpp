#include "Application.h"


Application::Application()
{
	cout << "Application: Constructor" << endl;
}

Application::~Application()
{
	docs.clear();
	docs.shrink_to_fit();
}

void Application::NewDocument(const string &name)
{
	cout << "Application: NewDocument" << endl;
	docs.push_back(CreateDocument(name));
	docs[docs.size() - 1]->Open();
}

void Application::ReportDocs()
{
	cout << "Application: ReportDocs" << endl;
	for (int doc = 0; doc < docs.size(); doc++)
	{
		cout << " " << docs[doc]->GetName() << endl;
	}
}
