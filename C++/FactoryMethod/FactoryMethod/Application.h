#pragma once
#include <vector>
using namespace std;

#include "Document.h"

class Application
{
	vector <Document*> docs;

protected:

	virtual Document* CreateDocument(const string& name) = 0;

public:
	Application();

	virtual ~Application();

	void NewDocument(const string &name);

	void OpenDocument() {}

	void ReportDocs();
};

