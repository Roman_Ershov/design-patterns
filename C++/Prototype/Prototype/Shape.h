#pragma once
#include <string>
using namespace std;

class Shape
{
protected:
	int X;
	int Y;
	string color;

public:
	Shape(int x, int y, string col);
	Shape(Shape* target);
	virtual ~Shape();

	virtual Shape* Clone() = 0;
};

