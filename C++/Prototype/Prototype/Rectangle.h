#pragma once
#include "Shape.h"
class Rectangle :
	public Shape
{
	int width;
	int height;

public:
	Rectangle(int x, int y, string col, int w, int h);

	Rectangle(Rectangle* target); 

	~Rectangle(){}

	Shape* Clone();
};

