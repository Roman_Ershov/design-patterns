#include "Shape.h"

Shape::Shape(int x, int y, string col)
{
	this->X = x;
	this->Y = y;
	this->color = col;
}

Shape::Shape(Shape* target)
{
	if (target != NULL)
	{
		this->X = target->X;
		this->Y = target->Y;
		this->color = target->color;
	}
}

Shape::~Shape()
{
	color.clear();
	color.shrink_to_fit();
}
