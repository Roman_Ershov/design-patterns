#include "Rectangle.h"
#include <iostream>
using namespace std;

Rectangle::Rectangle(int x, int y, string col, int w, int h) : Shape(x, y, col)
{
	this->width = w;
	this->height = h;
}

Rectangle::Rectangle(Rectangle* target) : Shape(target)
{
	if (target != NULL)
	{
		this->width = target->width;
		this->height = target->height;
	}

	cout << "Copy Rectangle: " << X << " " << Y << " " << color << " " << width << " " << height << endl;
}

Shape* Rectangle::Clone()
{
	return new Rectangle(this);
}
