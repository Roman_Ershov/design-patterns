#pragma once
#include "Shape.h"
class Circle :
	public Shape
{
	int radius;

public:
	Circle(int x, int y, string col, int r);

	Circle(Circle *target);

	~Circle(){}

	Shape* Clone();
};

