#include "Circle.h"
#include <iostream>
using namespace std;

Circle::Circle(int x, int y, string col, int r) : Shape(x, y, col)
{
	this->radius = r;
}

Circle::Circle(Circle *target) : Shape(target)
{
	if (target != NULL)
	{
		this->radius = target->radius;
	}

	cout << "Copy Circle: " << X << " " << Y << " " << color << " " << radius << endl;
}

Shape* Circle::Clone()
{
	return new Circle(this);
}