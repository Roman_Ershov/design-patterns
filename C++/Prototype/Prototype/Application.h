#pragma once
#include "Shape.h"
#include <vector>
using namespace std;

class Application
{
	vector <Shape*> shapes;
public:
	Application();
	~Application();

	vector <Shape*> getShapes();
};

