#include "Application.h"
#include "Circle.h"
#include "Rectangle.h"

Application::Application()
{
	Circle *circle = new Circle(10, 20, "black", 5);
	shapes.push_back(circle);

	Circle *anotherCircle = static_cast<Circle*>(circle->Clone());
	shapes.push_back(anotherCircle);

	Rectangle *rectangle = new Rectangle(5, 10, "red", 20, 15);
	shapes.push_back(rectangle);
}


Application::~Application()
{
	shapes.clear();
	shapes.shrink_to_fit();
}

vector <Shape*> Application::getShapes()
{
	return shapes;
}
