#pragma once
#include "Builder.h"

class Director
{
public:
	Director(){}
	~Director(){}

	void constructSportsCar(Builder *builder);
	void constructCityCar(Builder *builder);
	void constructSUV(Builder *builder);
};

