#include "Director.h"
#include "CarBuilder.h"
#include "CarManualBuilder.h"
#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
	Director *director = new Director;

	CarBuilder *builder = new CarBuilder();
	director->constructSportsCar(builder);
	Car* car = builder->getResult();
	cout << "Car built: \n" << car->getType();

	CarManualBuilder *manualBuilder = new CarManualBuilder();
	director->constructCityCar(manualBuilder);
	Manual* manual = manualBuilder->getResult();
	cout << "\nCar manual built: \n" << manual->print() << endl;

	delete director;
	delete builder;
	delete manualBuilder;

	return 0;
}