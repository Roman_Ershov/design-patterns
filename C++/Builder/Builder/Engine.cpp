#include "Engine.h"
#include <iostream>
using namespace std;

bool Engine::started;

void Engine::on()
{
	started = true;
}

void Engine::off()
{
	started = false;
}

bool Engine::isStarted()
{
	return isStarted;
}

void Engine::go(double mileage)
{
	if (started)
	{
		this->mileage += mileage;
	}
	else
	{
		cout << "Cannot go(), you nust start engine first!" << endl;
	}
}

double Engine::getVolume() const
{
	return volume;
}

double Engine::getMileage() const
{
	return mileage;
}