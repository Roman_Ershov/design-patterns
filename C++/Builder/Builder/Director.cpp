#include "Director.h"


void Director::constructSportsCar(Builder *builder)
{
	builder->setType(Type::CPORTS_CAR);
	builder->setSeats(2);
	builder->setEngine(new Engine(3.0, 0.0));
	builder->setTransmission(Transmission::SEMI_AUTOMATIC);
	builder->setGPSNavigator(new GPSNavigator());
	builder->setTripComputer(new TripComputer());
}

void Director::constructCityCar(Builder *builder)
{
	builder->setType(Type::CITY_CAR);
	builder->setSeats(2);
	builder->setEngine(new Engine(1.2, 0.0));
	builder->setTransmission(Transmission::AUTOMATIC);
	builder->setGPSNavigator(new GPSNavigator());
	builder->setTripComputer(new TripComputer());
}

void Director::constructSUV(Builder *builder)
{
	builder->setType(Type::SUV);
	builder->setSeats(4);
	builder->setEngine(new Engine(2.5, 0.0));
	builder->setTransmission(Transmission::MANUAL);
	builder->setGPSNavigator(new GPSNavigator());
}