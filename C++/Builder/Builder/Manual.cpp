#include "Manual.h"
#include <sstream>

string Manual::print() const
{
	stringstream info;
	info << "Type of car: " << type << "\n" <<
		"Count of seats: " << seats << "\n" <<
		"Engine: volume - " << engine->getVolume() << "; mileage - " << engine->getMileage() << "\n" <<
		"Transmisstion: " << transmission <<
		"Trip Computer: Trip Computer\n" <<
		"GPS Navigator: GPS Navigator\n";

	return info.str();
}