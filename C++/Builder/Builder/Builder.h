#pragma once
#include "Type.h"
#include "Engine.h"
#include "Transmission.h"
#include "GPSNavigator.h"
#include "TripComputer.h"

class Builder
{
public:
	virtual void setType(Type type) = 0;
	virtual void setSeats(int seats) = 0;
	virtual void setEngine(Engine *engine) = 0;
	virtual void setTransmission(Transmission transmission) = 0;
	virtual void setGPSNavigator(GPSNavigator *gpsNavigator) = 0;
	virtual void setTripComputer(TripComputer *tripComputer) = 0;
};