#include "GPSNavigator.h"


GPSNavigator::GPSNavigator()
{
	route = "221b, Baker Street, London  to Scotland Yard, 8-10 Broadway, London";
}

GPSNavigator::GPSNavigator(const string &manualRoute)
{
	route = manualRoute;
}

string GPSNavigator::getRoute()
{
	return route;
}

GPSNavigator::~GPSNavigator()
{
	route.clear();
	route.shrink_to_fit();
}
