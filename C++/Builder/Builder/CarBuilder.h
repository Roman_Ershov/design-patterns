#pragma once
#include "Builder.h"
#include "Car.h"

class CarBuilder :
	public Builder
{
	Type type;
	int seats;
	Engine *engine;
	Transmission transmission;
	GPSNavigator *gpsNavigator;
	TripComputer *tripComputer;

public:
	CarBuilder(){}
	~CarBuilder();

	virtual void setType(Type type);
	virtual void setSeats(int seats);
	virtual void setEngine(Engine *engine);
	virtual void setTransmission(Transmission transmission);
	virtual void setGPSNavigator(GPSNavigator *gpsNavigator);
	virtual void setTripComputer(TripComputer *tripComputer);

	Car* getResult();
};

