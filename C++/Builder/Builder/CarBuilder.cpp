#include "CarBuilder.h"


void CarBuilder::setType(Type type)
{
	this->type = type;
}

void CarBuilder::setSeats(int seats)
{
	this->seats = seats;
}

void CarBuilder::setEngine(Engine *engine)
{
	this->engine = engine;
}

void CarBuilder::setTransmission(Transmission transmission)
{
	this->transmission = transmission;
}

void CarBuilder::setGPSNavigator(GPSNavigator *gpsNavigator)
{
	this->gpsNavigator = gpsNavigator;
}

void CarBuilder::setTripComputer(TripComputer *tripComputer)
{
	this->tripComputer = tripComputer;
}

Car* CarBuilder::getResult()
{
	return new Car(type, seats, engine, transmission, gpsNavigator, tripComputer);
}

CarBuilder::~CarBuilder()
{
	delete engine;
	delete gpsNavigator;
	delete tripComputer;
}
