#include "CarManualBuilder.h"


void CarManualBuilder::setType(Type type)
{
	this->type = type;
}

void CarManualBuilder::setSeats(int seats)
{
	this->seats = seats;
}

void CarManualBuilder::setEngine(Engine *engine)
{
	this->engine = engine;
}

void CarManualBuilder::setTransmission(Transmission transmission)
{
	this->transmission = transmission;
}

void CarManualBuilder::setGPSNavigator(GPSNavigator *gpsNavigator)
{
	this->gpsNavigator = gpsNavigator;
}

void CarManualBuilder::setTripComputer(TripComputer *tripComputer)
{
	this->tripComputer = tripComputer;
}

Manual* CarManualBuilder::getResult()
{
	return new Manual(type, seats, engine, transmission, gpsNavigator, tripComputer);
}

CarManualBuilder::~CarManualBuilder()
{
	delete engine;
	delete gpsNavigator;
	delete tripComputer;
}
