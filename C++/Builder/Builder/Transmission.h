#pragma once

enum Transmission
{
	SINGLE_SPEED,
	MANUAL,
	AUTOMATIC,
	SEMI_AUTOMATIC
};