#include "Car.h"

double Car::fuel = 0.0;

void Car::setFuel(double fuel)
{
	this->fuel = fuel;
}

int Car::getSeats() const
{
	return seats;
}

Type Car::getType() const
{
	return type;
}

const Engine* Car::getEngine() const
{
	return engine;
}

Transmission Car::getTransmission() const
{
	return transmission;
}

const GPSNavigator* Car::getGPSNavigator() const
{
	return gpsNavigator;
}

const TripComputer* Car::getTripComputer() const
{
	return tripComputer;
}

double Car::getFuel()
{
	return fuel;
}