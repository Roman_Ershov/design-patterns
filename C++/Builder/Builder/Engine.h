#pragma once
class Engine
{
	const double volume;
	double mileage;
	static bool started;

public:
	Engine(double _volume, double _mileage) : volume(_volume), mileage(_mileage) {};
	~Engine() {}

	void on();
	void off();
	static bool isStarted();
	void go(double mileage);
	double getVolume() const;
	double getMileage() const;
};

