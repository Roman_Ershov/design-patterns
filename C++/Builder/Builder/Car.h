#pragma once
#include "Type.h"
#include "Engine.h"
#include "Transmission.h"
#include "GPSNavigator.h"
#include "TripComputer.h"
class Car
{
	const Type type;
	const int seats;
	const Engine *engine;
	const Transmission transmission;
	const GPSNavigator *gpsNavigator;
	const TripComputer *tripComputer;
	static double fuel;

public:
	Car(Type _type, int _seats, Engine *_engine, Transmission _transmission, GPSNavigator *_gpsNavigator, TripComputer *_tripComputer) :
		type(type), seats(_seats), engine(_engine), transmission(_transmission), gpsNavigator(_gpsNavigator), tripComputer(_tripComputer)
	{}
	~Car(){}

	void setFuel(double fuel);

	int getSeats() const;

	Type getType() const;

	const Engine* getEngine() const;

	Transmission getTransmission() const;

	const GPSNavigator* getGPSNavigator() const;

	const TripComputer* getTripComputer() const;

	static double getFuel();
};

