#pragma once
#include "Builder.h"
#include "Manual.h"

class CarManualBuilder :
	public Builder
{
	Type type;
	int seats;
	Engine *engine;
	Transmission transmission;
	GPSNavigator *gpsNavigator;
	TripComputer *tripComputer;
public:
	CarManualBuilder(){}
	~CarManualBuilder();

	virtual void setType(Type type);
	virtual void setSeats(int seats);
	virtual void setEngine(Engine *engine);
	virtual void setTransmission(Transmission transmission);
	virtual void setGPSNavigator(GPSNavigator *gpsNavigator);
	virtual void setTripComputer(TripComputer *tripComputer);

	Manual* getResult();
};

