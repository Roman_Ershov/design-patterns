#pragma once
#include "Type.h"
#include "Engine.h"
#include "Transmission.h"
#include "GPSNavigator.h"
#include "TripComputer.h"
#include <string>
using namespace std;

class Manual
{
	const Type type;
	const int seats;
	const Engine *engine;
	const Transmission transmission;
	const GPSNavigator *gpsNavigator;
	const TripComputer *tripComputer;

public:
	Manual(Type _type, int _seats, Engine *_engine, Transmission _transmission, GPSNavigator *_gpsNavigator, TripComputer *_tripComputer) :
		type(type), seats(_seats), engine(_engine), transmission(_transmission), gpsNavigator(_gpsNavigator), tripComputer(_tripComputer)
	{}
	~Manual(){}

	string print() const;
};

