#include "TripComputer.h"
#include "Engine.h"
#include "Car.h"
#include <iostream>
using namespace std;

void TripComputer::condition()
{
	if (Engine::isStarted())
	{
		cout << "Car is started" << endl;
	}
	else
	{
		cout << "Car is not started" << endl;
	}
}

void TripComputer::fuelLevel()
{
	cout << "Level of fuel - " << Car::getFuel();
}