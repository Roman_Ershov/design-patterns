#pragma once
#include <string>
using namespace std;

class GPSNavigator
{
	string route;

public:
	GPSNavigator();
	GPSNavigator(const string &manualRoute);
	~GPSNavigator();
	string getRoute();
};

