#pragma once
#include "Rectangle.h"
#include "LegacyRectangle.h"

class RectangleAdapter: public Rectangle, private LegacyRectangle
{
public:
	RectangleAdapter(Coordinate x, Coordinate y, Dimension w, Dimension h);
	virtual void draw();
};

