#include "LegacyRectangle.h"
#include <iostream>
using namespace std;

LegacyRectangle::LegacyRectangle(Coordinate x1, Coordinate y1, Coordinate x2, Coordinate y2)
{
	x1_ = x1;
	x2_ = x2;
	y1_ = y1;
	y2_ = y2;

	cout << "LegacyRectangle: create. (" << x1_ << "," << y1_ << ") => (" << x2_ << "," << y2_ << ")" << endl;
}

void LegacyRectangle::oldDraw()
{
	cout << "LegacyRectangle: oldDraw. (" << x1_ << "," << y1_ << ") => (" << x2_ << "," << y2_ << ")" << endl;
}
