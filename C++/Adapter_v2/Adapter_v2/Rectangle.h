#pragma once
typedef int Coordinate;
typedef int Dimension;

class Rectangle
{
public:
	virtual void draw() = 0;
};