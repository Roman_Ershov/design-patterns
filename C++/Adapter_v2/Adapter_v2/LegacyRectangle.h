#pragma once
#include "Rectangle.h"
class LegacyRectangle
{
	Coordinate x1_;
	Coordinate x2_;
	Coordinate y1_;
	Coordinate y2_;
public:
	LegacyRectangle(Coordinate x1, Coordinate y1, Coordinate x2, Coordinate y2);
	void oldDraw();
};

