#include "StackList.h"

void StackList::push(int i)
{
	if (last == nullptr)
	{
		last = new Node(i);
		last->prev = nullptr;
	}
	else
	{
		last->next = new Node(i);
		last->next->prev = last;
		last = last->next;
	}
}

int StackList::pop()
{
	if (isEmpty()) return -1;
	int ret = last->value;
	last = last->prev;
	return ret;
}

int StackList::top()
{
	if (isEmpty()) return -1;
	return last->value;
}

bool StackList::isEmpty()
{
	return last == nullptr;
}

bool StackList::isFull()
{
	return false;
}

StackList::~StackList()
{
	if (last != nullptr)  delete last;
}