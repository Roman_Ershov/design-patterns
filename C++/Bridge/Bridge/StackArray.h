#pragma once
#include "StackImpl.h"
class StackArray :
	public StackImpl
{
	int items[12];
	int total;
public:
	StackArray();
	void push(int in);
	int pop();
	int top();
	bool isEmpty();
	bool isFull();
};

