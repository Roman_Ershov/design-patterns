#include "Stack.h"
#include "StackImpl.h"
#include "StackFIFO.h"
#include "StackHanoi.h"
#include <iostream>
using namespace std;
#include <time.h>

void main()
{
	srand(time(NULL));
	Stack* stacks[4] = {new Stack("array"), new Stack("list"), new StackFIFO(), new StackHanoi()};

	for (int i = 1; i < 15; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			stacks[j]->push(i);
		}
	}

	for (int i = 1; i < 15; i++)
	{
		stacks[3]->push((double)rand() / RAND_MAX * 20);
	}

	for (int i = 0; i < 4; i++)
	{
		while (!stacks[i]->isEmpty())
		{
			cout << stacks[i]->pop() << " ";
		}
		cout << endl;
	}

	cout << "total rejected is " << ((StackHanoi *)stacks[3])->reportRejected() << endl;
}