#pragma once
#include <string>
using namespace std;
#include "StackImpl.h"

class Stack
{
	StackImpl *impl;
public:
	Stack(string s);
	Stack();
	virtual void push(int in);
	virtual int top();
	virtual int pop();
	virtual bool isEmpty();
	virtual bool isFull();
	virtual ~Stack();
};

