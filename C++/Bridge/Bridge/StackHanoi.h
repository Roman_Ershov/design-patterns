#pragma once
#include "Stack.h"

class StackHanoi :
	public Stack
{
	int totalRejected;
public:
	StackHanoi();
	StackHanoi(string s);
	int reportRejected();
	void push(int in);
};

