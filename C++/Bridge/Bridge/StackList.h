#pragma once
#include "StackImpl.h"

struct Node
{
	int value;
	Node *prev, *next;
	Node(int i) : value(i){}
};

class StackList :
	public StackImpl
{
	Node *last = nullptr;
public:
	
	void push(int i);
	int pop();
	int top();
	bool isEmpty();
	bool isFull();
	~StackList();
};

