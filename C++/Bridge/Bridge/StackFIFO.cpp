#include "StackFIFO.h"
#include "StackList.h"

StackFIFO::StackFIFO() : Stack("array")
{
	temp = new StackList();
}

StackFIFO::StackFIFO(string s) : Stack(s)
{
	temp = new StackList();
}

int StackFIFO::pop()
{
	while (!isEmpty())
	{
		temp->push(Stack::pop());
	}
	int ret = temp->pop();
	while (!temp->isEmpty())
	{
		push(temp->pop());
	}
	return ret;
}

StackFIFO::~StackFIFO()
{
	delete temp;
}
