#include "Stack.h"
#include <iostream>
using namespace std;
#include "StackArray.h"
#include "StackList.h"

Stack::Stack(string s)
{
	if (s == "array")
	{
		impl = new StackArray();
	}
	else if (s == "list")
	{
		impl = new StackList();
	}
	else
	{
		cout << "Stack: Unknown Parameter" << endl;
	}
}

Stack::Stack() 
{
	impl = new StackArray();
}

void Stack::push(int in)
{
	impl->push(in);
}

int Stack::top()
{
	return impl->top();
}

int Stack::pop()
{
	return impl->pop();
}

bool Stack::isEmpty()
{
	return impl->isEmpty();
}

bool Stack::isFull()
{
	return impl->isFull();
}

Stack::~Stack()
{
	delete impl;
}