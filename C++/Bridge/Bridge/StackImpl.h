#pragma once

class StackImpl
{
public:
	virtual void push(int in) = 0;
	virtual int pop() = 0;
	virtual int top() = 0;
	virtual bool isEmpty() = 0;
	virtual bool isFull() = 0;
};