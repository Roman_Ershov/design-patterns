#pragma once
#include "Stack.h"

class StackFIFO :
	public Stack
{
	StackImpl *temp;
public:
	StackFIFO();
	StackFIFO(string s);
	int pop();
	~StackFIFO();
};

