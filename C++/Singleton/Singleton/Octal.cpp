#include "Octal.h"
#include <sstream>

void Octal::setValue(int val)
{
	stringstream stream;
	stream << oct << val;
	string oct_str = stream.str();

	stringstream in;
	in << dec << oct_str;
	in >> this->value;
}

