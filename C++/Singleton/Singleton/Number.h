#pragma once
#include <string>
using namespace std;

class Number
{
	static string type;
	static Number *instance;

protected:
	int value;
	Number();

public:
	static Number* getInstance();
	static void setType(string t);
	virtual void setValue(int val);
	virtual int getValue();
};

