#include "Number.h"
#include <iostream>

void main()
{
	Number::getInstance()->setValue(42);
	cout << "value is " << Number::getInstance()->getValue() << endl;
	Number::setType("octal");
	Number::getInstance()->setValue(64);
	cout << "value is " << Number::getInstance()->getValue() << endl;
}