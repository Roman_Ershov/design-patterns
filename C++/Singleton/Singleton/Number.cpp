#include "Number.h"
#include "Octal.h"
#include <iostream>

string Number::type = "decimal";
Number* Number::instance = NULL;

Number::Number()
{
	cout << "Number constructor" << endl;
}

void Number::setType(string t)
{
	type = t;
	delete instance;
	instance = NULL;
}

void Number::setValue(int val)
{
	value = val;
}

int Number::getValue()
{
	return value;
}

Number* Number::getInstance()
{
	if (instance == NULL)
	{
		if (type == "octal")
		{
			instance = new Octal();
		}
		else
		{
			instance = new Number();
		}
	}

	return instance;
}

