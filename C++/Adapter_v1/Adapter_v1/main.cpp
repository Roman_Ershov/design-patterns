#include "RoundHole.h"
#include "RoundPeg.h"
#include "SquarePegAdapter.h"

void main()
{
	RoundHole *hole = new RoundHole(5);
	RoundPeg *rpeg = new RoundPeg(5);
	hole->fits(rpeg);

	SquarePeg *smallPeg = new SquarePeg(2);
	SquarePeg *largePeg = new SquarePeg(5);
	//hole->fits(smallPeg);
	//hole->fits(largePeg);

	SquarePegAdapter *smallPegAdapter = new SquarePegAdapter(smallPeg);
	SquarePegAdapter *largePegAdapter = new SquarePegAdapter(largePeg);
	hole->fits(smallPegAdapter);
	hole->fits(largePegAdapter);

	delete hole;
	delete rpeg;
	delete smallPeg;
	delete largePeg;
	delete smallPegAdapter;
	delete largePegAdapter;
}