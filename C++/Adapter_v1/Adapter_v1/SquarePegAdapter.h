#pragma once
#include "RoundPeg.h"
#include "SquarePeg.h"
class SquarePegAdapter :
	public RoundPeg
{
	SquarePeg *peg;

public:
	SquarePegAdapter(SquarePeg *peg);

	int getRadius();
};

