#pragma once
#include "RoundPeg.h"

class RoundHole
{
	int radius;
public:
	RoundHole(int radius);

	int getRadius();

	bool fits(RoundPeg *peg);
};

