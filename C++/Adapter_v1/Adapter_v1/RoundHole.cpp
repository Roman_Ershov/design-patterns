#include "RoundHole.h"
#include <iostream>
using namespace std;

RoundHole::RoundHole(int radius)
{
	this->radius = radius;
}

int RoundHole::getRadius()
{
	return radius;
}

bool RoundHole::fits(RoundPeg *peg)
{
	cout << "Fit peg r = " << peg->getRadius() << " in hole r = " << radius << endl;
	bool success = radius >= peg->getRadius();
	if (success) cout << "Success" << endl;
	else cout << "Fail" << endl;

	return success;
} 