#include "SquarePegAdapter.h"
#include <math.h>

SquarePegAdapter::SquarePegAdapter(SquarePeg *peg)
{
	this->peg = peg;
}

int SquarePegAdapter::getRadius()
{
	return (int)sqrt(pow((peg->getWidth() / 2.0), 2.0) * 2.0);
}