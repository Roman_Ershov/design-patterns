#pragma once
#include "Button.h"
#include "CheckBox.h"
#include "GUIFactory.h"

class Application
{
	Button *button;
	CheckBox *checkBox;
	GUIFactory *factory;

public:
	Application(GUIFactory *_factory);
	void paint();
	~Application();
};