#pragma once
#include "GUIFactory.h"

class OSXFactory : public GUIFactory
{
public:
	virtual Button* createButton();
	virtual CheckBox* createCheckBox();

	OSXFactory(){}

	~OSXFactory(){}
};