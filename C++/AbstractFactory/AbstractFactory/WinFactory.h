#pragma once
#include "GUIFactory.h"

class WinFactory : public GUIFactory
{
public:
	virtual Button* createButton();
	virtual CheckBox* createCheckBox();

	WinFactory(){}

	~WinFactory(){}
};