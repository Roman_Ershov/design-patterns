#include "Application.h"
#include "OSXFactory.h"
#include "WinFactory.h"

Application* configureApp()
{
	Application *app;
	GUIFactory *factory;

#if _WIN32
	factory = new WinFactory();
#else
	factory = new OSXFactory();
#endif

	app = new Application(factory);
	return app;
}

int main(int argc, char* argv[])
{
	Application *app = configureApp();
	app->paint();

	delete app;
}