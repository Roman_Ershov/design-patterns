#include "WinFactory.h"
#include "WindowsButton.h"
#include "WindowsCheckBox.h"

Button* WinFactory::createButton()
{
	return new WindowsButton();
}

CheckBox* WinFactory::createCheckBox()
{
	return new WindowsCheckBox();
}