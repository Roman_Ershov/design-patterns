#include "Application.h"

Application::Application(GUIFactory *_factory)
{
	this->factory = _factory;
	button = factory->createButton();
	checkBox = factory->createCheckBox();
}

void Application::paint()
{
	button->paint();
	checkBox->paint();
}

Application::~Application()
{
	delete button;
	delete checkBox;
	delete factory;
}