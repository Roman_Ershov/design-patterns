#pragma once
#include "Button.h"
#include "CheckBox.h"

class GUIFactory
{
public:
	virtual Button* createButton() = 0;
	virtual CheckBox* createCheckBox() = 0;

	virtual ~GUIFactory() {};
};