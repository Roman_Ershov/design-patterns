#include "OSXFactory.h"
#include "OSXButton.h"
#include "OSXCheckBox.h"

Button* OSXFactory::createButton()
{
	return new OSXButton();
}

CheckBox* OSXFactory::createCheckBox()
{
	return new OSXCheckBox();
}