#pragma once
class CheckBox
{
public:
	virtual void paint() = 0;

	virtual ~CheckBox() {};
};