#include "Column.h"
#include <iostream>
using namespace std;

Column::Column(int val) : Composite(val)
{}

void Column::traverse()
{
	cout << "Col";
	Composite::traverse();
}