#pragma once
#include "Component.h"
#include <vector>
using namespace std;

class Composite : public Component
{
	vector <Component *> children;
	int value;

public:
	Composite(int val);

	void add(Component *c);

	void traverse();
};

