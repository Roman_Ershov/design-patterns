#include "Row.h"
#include "Column.h"
#include "Primitive.h"
#include <iostream>
using namespace std;

void main()
{
	Row first(1);
	Column second(2);
	Column third(3);
	Row fourth(4);
	Row fifth(5);
	first.add(&second);
	first.add(&third);
	third.add(&fourth);
	third.add(&fifth);
	first.add(&Primitive(6));
	second.add(&Primitive(7));
	third.add(&Primitive(8));
	fourth.add(&Primitive(9));
	fifth.add(&Primitive(10));
	first.traverse();
	cout << endl;
}