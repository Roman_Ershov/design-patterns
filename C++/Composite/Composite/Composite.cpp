#include "Composite.h"
#include <iostream>
using namespace std;

Composite::Composite(int val) : value(val)
{}

void Composite::add(Component *c)
{
	children.push_back(c);
}

void Composite::traverse()
{
	cout << value << " ";
	for (int i = 0; i < children.size(); i++)
	{
		children[i]->traverse();
	}
}