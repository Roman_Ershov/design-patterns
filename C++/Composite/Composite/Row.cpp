#include "Row.h"
#include <iostream>
using namespace std;

Row::Row(int val) : Composite(val)
{}

void Row::traverse()
{
	cout << "Row";
	Composite::traverse();
}