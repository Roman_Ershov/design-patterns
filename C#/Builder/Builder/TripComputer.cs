﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cars;

namespace Components
{
    class TripComputer
    {
        public void condition()
        {
            if (Engine.isStarted())
            {
                Console.WriteLine("Car is started");
            }
            else
            {
                Console.WriteLine("Car isn't started");
            }
        }

        public void fuelLevel()
        {
            Console.WriteLine("Level of fuel - {0}", Car.getFuel());
        }
    }
}
