﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Components;
using Cars;

namespace Builders
{
    interface IBuilder
    {
        void setType(Cars.Type type);
        void setSeats(int seats);
        void setEngine(Engine engine);
        void setTransmission(Transmission transmission);
        void setGPSNavigator(GPSNavigator gpsNavigator);
        void setTripComputer(TripComputer tripComputer);
    }
}
