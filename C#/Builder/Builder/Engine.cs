﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Components
{
    class Engine
    {
        private readonly double volume;
        private double mileage;
        private static bool started;

        public Engine(double volume, double mileage)
        {
            this.volume = volume;
            this.mileage = mileage;
        }

        public void on()
        {
            started = true;
        }

        public void off()
        {
            started = false;
        }

        public static bool isStarted()
        {
            return started;
        }

        public void go(double mileage)
        {
            if (started)
            {
                this.mileage += mileage;
            }
            else
            {
                Console.WriteLine("Cannot go(), you must start engine first!");
            }
        }

        public double getVolume()
        {
            return volume;
        }

        public double getMileage()
        {
            return mileage;
        }
    }
}
