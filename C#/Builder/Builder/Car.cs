﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Components;

namespace Cars
{
    class Car
    {
        private readonly Type type;
        private readonly int seats;
        private readonly Engine engine;
        private readonly Transmission transmission;
        private readonly GPSNavigator gpsNavigator;
        private readonly TripComputer tripComputer;
        private static double fuel = 0.0;

        public Car(Type type, int seats, Engine engine, Transmission transmission, GPSNavigator gpsNavigator, TripComputer tripComputer)
        {
            this.type = type;
            this.seats = seats;
            this.engine = engine;
            this.transmission = transmission;
            this.gpsNavigator = gpsNavigator;
            this.tripComputer = tripComputer;
        }

        public void setFuel(double _fuel)
        {
            fuel = _fuel;
        }

        public static double getFuel()
        {
            return fuel;
        }

        public Type getType() 
        {
            return type;
        }

        public int getSeats()
        {
            return seats;
        }

        public Engine getEngine()
        {
            return engine;
        }

        public Transmission getTransmission()
        {
            return transmission;
        }

        public GPSNavigator getGPSNavigator()
        {
            return gpsNavigator;
        }

        public TripComputer getTripComputer() 
        {
            return tripComputer;
        }
    }
}
