﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Director;
using Builders;
using Cars;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            Director.Director director = new Director.Director();

            CarBuilder builder = new CarBuilder();
            director.constructSportsCar(builder);

            Car car = builder.getResult();
            Console.WriteLine("Car built: \n{0}", car.getType());

            CarManualBuilder manualBuilder = new CarManualBuilder();
            director.constructCityCar(manualBuilder);
            Manual carManual = manualBuilder.getResult();
            Console.WriteLine("\nCar manual built: \n" + carManual.print());
        }
    }
}
