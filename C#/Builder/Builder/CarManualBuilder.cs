﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Components;
using Cars;

namespace Builders
{
    class CarManualBuilder: IBuilder
    {
        private Cars.Type type;
        private int seats;
        private Engine engine;
        private Transmission transmission;
        private GPSNavigator gpsNavigator;
        private TripComputer tripComputer;

        public void setType(Cars.Type type)
        {
            this.type = type;
        }

        public void setSeats(int seats)
        {
            this.seats = seats;
        }

        public void setEngine(Engine engine)
        {
            this.engine = engine;
        }

        public void setTransmission(Transmission transmission)
        {
            this.transmission = transmission;
        }

        public void setGPSNavigator(GPSNavigator gpsNavigator)
        {
            this.gpsNavigator = gpsNavigator;
        }

        public void setTripComputer(TripComputer tripComputer)
        {
            this.tripComputer = tripComputer;
        }

        public Manual getResult()
        {
            return new Manual(type, seats, engine, transmission, gpsNavigator, tripComputer);
        }
    }
}
