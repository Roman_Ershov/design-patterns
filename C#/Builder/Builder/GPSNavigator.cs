﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Components
{
    class GPSNavigator
    {
        private String route;

        public GPSNavigator()
        {
            route = "221b, Baker Street, London  to Scotland Yard, 8-10 Broadway, London";
        }

        public GPSNavigator(String manualRoute)
        {
            this.route = manualRoute;
        }

        public String getRoute()
        {
            return route;
        }
    }
}
