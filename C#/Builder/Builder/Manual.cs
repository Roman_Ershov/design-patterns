﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Components;

namespace Cars
{
    class Manual
    {
        private readonly Type type;
        private readonly int seats;
        private readonly Engine engine;
        private readonly Transmission transmission;
        private readonly GPSNavigator gpsNavigator;
        private readonly TripComputer tripComputer;
        
         public Manual(Type type, int seats, Engine engine, Transmission transmission, GPSNavigator gpsNavigator, TripComputer tripComputer)
        {
            this.type = type;
            this.seats = seats;
            this.engine = engine;
            this.transmission = transmission;
            this.gpsNavigator = gpsNavigator;
            this.tripComputer = tripComputer;
        }

         public String print()
         {
             StringBuilder info = new StringBuilder();
             info.Append("Type of car: " + type + "\n");
             info.Append("Count of seats: " + seats + "\n");
             info.Append("Engine: volume - " + engine.getVolume() + "; mileage - " + engine.getMileage() + "\n");
             info.Append("Transmission: " + transmission + "\n");
             info.Append("Trip Computer: Trip Computer" + "\n");
             info.Append("GPS Navigator: GPS Navigator" + "\n");
             return info.ToString();
         }
    }
}
