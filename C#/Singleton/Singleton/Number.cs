﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers
{
    class Number
    {
        private static string type = "decimal";
        private static Number instance = null;

        protected int value;
        protected Number()
        {
            Console.WriteLine("Number constructor");
        }

        public static Number GetIstance()
        {
            if (instance == null)
            {
                if (type == "octal")
                {
                    instance = new Octal();
                }
                else
                {
                    instance = new Number();
                }
            }

            return instance;
        }

        public static void SetType(string t)
        {
            type = t;
            instance = null;
        }

        public virtual void SetValue(int value)
        {
            this.value = value;
        }

        public virtual int GetValue()
        {
            return value;
        }
    }
}
