﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            Number.GetIstance().SetValue(42);
            Console.WriteLine("value is {0}", Number.GetIstance().GetValue());
            Number.SetType("octal");
            Number.GetIstance().SetValue(64);
            Console.WriteLine("value is {0}", Number.GetIstance().GetValue());
        }
    }
}
