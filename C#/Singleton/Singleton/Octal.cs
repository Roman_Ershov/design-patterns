﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers
{
    class Octal : Number
    {
        
        internal protected Octal()
        {}

        public override void SetValue(int value)
        {
            string oct_val = Convert.ToString(value, 8);
            this.value = Convert.ToInt32(oct_val);
        }
    }
}
