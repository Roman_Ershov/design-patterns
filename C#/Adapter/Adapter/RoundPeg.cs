﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class RoundPeg
    {
        private int raduis;

        public RoundPeg() { }

        public RoundPeg(int r)
        {
            this.raduis = r;
        }

        public virtual int GetRadius()
        {
            return raduis;
        }
    }
}
