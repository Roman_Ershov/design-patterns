﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            RoundHole hole = new RoundHole(5);
            RoundPeg peg = new RoundPeg(5);
            if (hole.Fits(peg)) Console.WriteLine("Success");
            else Console.WriteLine("Fail");

            SquarePeg smallPeg = new SquarePeg(2);
            SquarePeg largePeg = new SquarePeg(5);
            //hole.Fits(smallPeg);
            //hole.Fits(largePeg);

            SquarePegAdapter smallPegAdapter = new SquarePegAdapter(smallPeg);
            SquarePegAdapter largePegAdapter = new SquarePegAdapter(largePeg);
            if (hole.Fits(smallPegAdapter)) Console.WriteLine("Success");
            else Console.WriteLine("Fail");
            if (hole.Fits(largePegAdapter)) Console.WriteLine("Success");
            else Console.WriteLine("Fail");
        }
    }
}
