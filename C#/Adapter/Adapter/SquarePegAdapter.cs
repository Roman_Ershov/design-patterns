﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class SquarePegAdapter: RoundPeg
    {
        private SquarePeg peg;

        public SquarePegAdapter(SquarePeg peg)
        {
            this.peg = peg;
        }

        public override int GetRadius()
        {
            return (int)Math.Sqrt(Math.Pow((peg.GetWidth() / 2.0), 2.0) * 2.0);
        }


    }
}
