﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class RoundHole
    {
        private int radius;

        public RoundHole(int radius)
        {
            this.radius = radius;
        }

        public int GetRadius()
        {
            return radius;
        }

        public bool Fits(RoundPeg peg)
        {
            Console.WriteLine("Fit peg r = {0} in Hole r = {1}", peg.GetRadius(), radius);
            return radius >= peg.GetRadius();
        }
    }
}
