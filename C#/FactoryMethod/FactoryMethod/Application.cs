﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documents;

namespace Applications  
{
    abstract class Application
    {
        private int index;
        private List<Document> docs = new List<Document>();

        public abstract Document CreateDocument(String name);

        public void OpenDocuments() { }

        public void ReportDocs()
        {
            Console.WriteLine("Application: ReportDocs");
            foreach(Document doc in docs)
            {
                Console.WriteLine(" " + doc.getName());
            }
        }

        public void NewDocument(String name)
        {
            Console.WriteLine("Application: NewDocument");
            docs.Add(CreateDocument(name));
            docs[docs.Count - 1].Open();
        }

        public Application() 
        {
            index = 0;
            Console.WriteLine("Application: constructor");
        }
    }
}
