﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Applications;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            WordApplication app = new WordApplication();
            for (int i = 0; i < 5; i++)
            {
                StringBuilder name = new StringBuilder();
                name.Append("Document");
                name.Append((i + 1).ToString());
                name.Append(".doc");

                app.NewDocument(name.ToString());
            }

            app.ReportDocs();
        }
    }
}
