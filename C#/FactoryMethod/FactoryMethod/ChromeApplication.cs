﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documents;

namespace Applications
{
    class ChromeApplication: Application
    {
        public ChromeApplication()
        {
            Console.WriteLine("ChromeApplication: Constructor");
        }

        public override Document CreateDocument(string name)
        {
            return new HtmlDocument(name);
        }
    }
}
