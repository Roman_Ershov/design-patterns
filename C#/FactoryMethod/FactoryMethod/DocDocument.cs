﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documents
{
    class DocDocument: Document
    {
        public DocDocument(String fn): base(fn) {}

        public override void Open() 
        {
            Console.WriteLine("DocDocument: open");
        }

        public override void Close()
        {
            Console.WriteLine("DocDocument: close");
        }

    }
}
