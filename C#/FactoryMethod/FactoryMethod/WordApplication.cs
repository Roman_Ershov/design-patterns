﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documents;

namespace Applications
{
    class WordApplication: Application
    {
        public WordApplication()
        {
            Console.WriteLine("WordApplication Constructor");
        }

        public override Document CreateDocument(string name)
        {
            return new DocDocument(name);
        } 
    }
}
