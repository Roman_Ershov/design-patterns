﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documents
{
    abstract class Document
    {
        private String name;

        public Document(String fn)
        {
            name = fn;
        }

        public abstract void Open();

        public abstract void Close();

        public String getName()
        {
            return name;
        }
    }
}
