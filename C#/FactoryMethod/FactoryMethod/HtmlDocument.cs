﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documents
{
    class HtmlDocument: Document
    {
        public HtmlDocument(String fn) : base(fn) { }

        public override void Open()
        {
            Console.WriteLine("HtmlDocument: open");
        }

        public override void Close()
        {
            Console.WriteLine("HtmlDocument: close");
        }
    }
}
