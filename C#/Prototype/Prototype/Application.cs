﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shapes;

namespace Prototype
{
    class Application
    {
        private List<Shape> shapes = new List<Shape>();

        public Application()
        {
            Circle circle = new Circle(10, 20, "black", 15);
            shapes.Add(circle);
            Circle anotherCircle = (Circle)circle._Clone();
            shapes.Add(anotherCircle);

            Rectangle rectangle = new Rectangle(5, 10, "red", 10, 20);
            shapes.Add(rectangle);
        }

        public List<Shape> GetShapes()
        {
            return shapes;
        }
    }
}
