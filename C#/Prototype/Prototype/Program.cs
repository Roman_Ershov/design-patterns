﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shapes;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Application app = new Application();
            List<Shape> shapes = app.GetShapes();

            List<Shape> shapes_copy = new List<Shape>();
            foreach (Shape shape in shapes)
            {
                shapes_copy.Add(shape._Clone());
            }
        }
    }
}
