﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    abstract class Shape
    {
        protected int X;
        protected int Y;
        protected string color;

        public Shape(int x, int y, string col)
        {
            this.X = x;
            this.Y = y;
            this.color = col;
        }

        public Shape(Shape target)
        {
            if (target != null)
            {
                this.X = target.X;
                this.Y = target.Y;
                this.color = target.color;
            }
        }

        public abstract Shape _Clone();
    }
}
