﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Circle: Shape
    {
        private int radius;

        public Circle(int x, int y, string color, int r): base(x, y, color)
        {
            this.radius = r;
        }

        public Circle(Circle target): base(target)
        {
            if (target != null)
            {
                this.radius = target.radius;
            }

            Console.WriteLine("Copy Circle: {0}, {1}, {2}, {3}", X, Y, color, radius);
        }

        public override Shape _Clone()
        {
            return new Circle(this);
        }

    }
}
