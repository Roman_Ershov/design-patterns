﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Rectangle: Shape
    {
        private int width;
        private int height;

        public Rectangle(int x, int y, string col, int w, int h)
            : base(x, y, col)
        {
            this.width = w;
            this.height = h;
        }

        public Rectangle(Rectangle target): base(target)
        {
            if (target != null)
            {
                this.width = target.width;
                this.height = target.height;
            }
            Console.WriteLine("Copy Rectangle: {0}, {1}, {2}, {3}, {4}", X, Y, color, width, height);
        }

        public override Shape _Clone()
        {
            return new Rectangle(this);
        }
    }
}
