﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    class Box : Entity
    {
        private List<Entity> children = new List<Entity>();
        private int value;

        public Box(int val)
        {
            value = val;
        }

        public void Add(Entity c)
        {
            children.Add(c);
        }

        public override void traverse()
        {
            Console.WriteLine(indent.ToString() + "{0}", value);
            indent.Append(" ");

            for (int i = 0; i < children.Count; i++)
            {
                ((Entity)children[i]).traverse();
            }
            if (indent.Length >= 3) indent.Length = indent.Length - 3;
        }
    }
}
