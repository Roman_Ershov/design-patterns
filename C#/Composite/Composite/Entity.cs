﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    abstract class Entity
    {
        protected static StringBuilder indent = new StringBuilder();
        public abstract void traverse();
    }
}
