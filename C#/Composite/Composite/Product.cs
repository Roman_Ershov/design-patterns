﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    class Product : Entity
    {
        private int value;
        public Product(int val)
        {
            value = val;
        }

        public override void traverse()
        {
            Console.WriteLine(indent.ToString() + "{0}", value);
        }
    }
}
