﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackImpls
{
    class StackList: StackImpl
    {
        private Node last;

        public void push(int i)
        {
            if (last == null)
            {
                last = new Node(i);
            }
            else
            {
                last.next = new Node(i);
                last.next.prev = last;
                last = last.next;
            }
        }

        public bool isEmpty()
        {
            return last == null;
        }

        public bool isFull()
        {
            return false;
        }

        public int top()
        {
            if (last == null) return -1;
            return last.value;
        }

        public int pop()
        {
            if (isEmpty()) return -1;
            int ret = last.value;
            last = last.prev;
            return ret;
        }
    }
}
