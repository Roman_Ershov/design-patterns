﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackImpls;

namespace Stacks
{
    class Stack
    {
        protected StackImpl impl;

        public Stack(String s)
        {
            if (s.Equals("array"))
                impl = new StackArray();
            else if (s.Equals("list"))
                impl = new StackList();
            else Console.WriteLine("Stack: unknown parameter");
        }

        public Stack(): this("array")
        {}

        public virtual void push(int in_)
        {
            impl.push(in_);
        }

        public virtual int pop()
        {
            return impl.pop();
        }

        public int top()
        {
            return impl.top();
        }

        public bool isEmpty()
        {
            return impl.isEmpty();
        }

        public bool isFull()
        {
            return impl.isFull();
        }
    }   
}
