﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackImpls
{
    interface StackImpl
    {
        void push(int i);
        int pop();
        int top();
        bool isEmpty();
        bool isFull();
    }
}
