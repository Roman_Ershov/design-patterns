﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackImpls;

namespace Stacks
{
    class StackFIFO: Stack
    {
        private StackImpl temp = new StackList();
        public StackFIFO() : base("array") { }
        public StackFIFO(string s) : base(s) { }
        public override int pop()
        {
            while (!isEmpty())
            {
                temp.push(base.pop());
            }

            int ret = temp.pop();
            while (!temp.isEmpty())
            {
                push(temp.pop());
            }

            return ret;
        }
    }
}
