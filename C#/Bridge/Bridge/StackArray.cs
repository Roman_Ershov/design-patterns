﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackImpls
{
    class StackArray : StackImpl
    {
        private int[] items = new int[12];
        private int total = -1;

        public void push(int i)
        {
            if (!isFull())
            {
                items[++total] = i;
            }
        }

        public bool isEmpty()
        {
            return total == -1;
        }

        public bool isFull()
        {
            return total == 11;
        }

        public int top()
        {
            if (isEmpty()) return -1;
            return items[total];
        }

        public int pop()
        {
            if (isEmpty()) return -1;
            return items[total--];
        }
    }
}
