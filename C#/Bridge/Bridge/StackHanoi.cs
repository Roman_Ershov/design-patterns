﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stacks
{
    class StackHanoi: Stack
    {
        private int totalRejected = 0;
        public StackHanoi(): base("array"){}
        public StackHanoi(string s) : base(s) { }
        public int reportRejected()
        {
            return totalRejected;
        }
        public override void push(int in_)
        {
            if (!isEmpty() && in_ > top())
            {
                totalRejected++;
            }
            else
            {
                base.push(in_);
            }
        }
    }
}
