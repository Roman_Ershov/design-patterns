﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buttons;
using CheckBoxes;

namespace Factories
{
    class OSXFactory: GUIFactory
    {
        public Button createButton()
        {
            return new OSXButton();
        }

        public CheckBox createCheckBox()
        {
            return new OSXCheckBox();
        }
    }
}
