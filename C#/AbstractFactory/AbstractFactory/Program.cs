﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Factories;

namespace AbstractFactory
{
    class Program
    {
        static Application configureApp()
        {
            Application app = null;
            GUIFactory factory;

            String os = Environment.OSVersion.VersionString;
            String[] parse = os.Split(' ');
            if (parse[0].Equals("Microsoft"))
            {
                factory = new WindowsFactory();
            }
            else
            {
                factory = new OSXFactory();
            }
            app = new Application(factory);
            return app;
        }

        static void Main(string[] args)
        {
            Application app = configureApp();
            app.paint();
        }
    }
}
