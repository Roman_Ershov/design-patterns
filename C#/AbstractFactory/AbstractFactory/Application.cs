﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buttons;
using CheckBoxes;
using Factories;

namespace AbstractFactory
{
    class Application
    {
        Button button;
        CheckBox checkBox;

        public Application(GUIFactory factory)
        {
            button = factory.createButton();
            checkBox = factory.createCheckBox();
        }

        public void paint()
        {
            button.paint();
            checkBox.paint();
        }
    }
}
