﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buttons;
using CheckBoxes;

namespace Factories
{
    class WindowsFactory: GUIFactory
    {
        public Button createButton()
        {
            return new WindowsButton();
        }
        public CheckBox createCheckBox()
        {
            return new WindowsCheckBox();
        }
    }
}
