﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buttons;
using CheckBoxes;

namespace Factories
{
    interface GUIFactory
    {
        Button createButton();
        CheckBox createCheckBox();
    }
}
